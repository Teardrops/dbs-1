CREATE TABLE schema_migrations
(
  version VARCHAR NOT NULL
    CONSTRAINT schema_migrations_pkey
    PRIMARY KEY
);

CREATE TABLE ar_internal_metadata
(
  key        VARCHAR   NOT NULL
    CONSTRAINT ar_internal_metadata_pkey
    PRIMARY KEY,
  value      VARCHAR,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL
);

CREATE TABLE stations
(
  id          BIGSERIAL NOT NULL
    CONSTRAINT stations_pkey
    PRIMARY KEY,
  label       VARCHAR,
  ticket_sale BOOLEAN,
  created_at  TIMESTAMP NOT NULL,
  updated_at  TIMESTAMP NOT NULL
);

CREATE TABLE stops
(
  id         BIGSERIAL NOT NULL
    CONSTRAINT stops_pkey
    PRIMARY KEY,
  "order"    INTEGER,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL,
  station_id INTEGER
    CONSTRAINT fk_rails_3c812e5e58
    REFERENCES stations,
  track_id   INTEGER
    CONSTRAINT fk_rails_3c812e5e58
    REFERENCES tracks,
);

CREATE TABLE passengers
(
  id            BIGSERIAL NOT NULL
    CONSTRAINT passengers_pkey
    PRIMARY KEY,
  first_name    VARCHAR,
  last_name     VARCHAR,
  birth_date    DATE,
  entry_station INTEGER,
  exit_station  INTEGER,
  created_at    TIMESTAMP NOT NULL,
  updated_at    TIMESTAMP NOT NULL,
);

CREATE TABLE ways
(
  id         BIGSERIAL NOT NULL
    CONSTRAINT ways_pkey
    PRIMARY KEY,
  delay      INTEGER,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL,
  track_id   INTEGER
    CONSTRAINT fk_rails_c6fc2445d8
    REFERENCES tracks
  locomotive_id INTEGER
    CONSTRAINT fk_rails_c6fc2445d8
    REFERENCES locomotives
);

CREATE TABLE wagons
(
  id              BIGSERIAL NOT NULL
    CONSTRAINT wagons_pkey
    PRIMARY KEY,
  wagon_type      VARCHAR,
  production_year INTEGER,
  created_at      TIMESTAMP NOT NULL,
  updated_at      TIMESTAMP NOT NULL,
);

CREATE TABLE wagon_usages
(
  from      DATETIME,
  to DATETIME,
  created_at      TIMESTAMP NOT NULL,
  updated_at      TIMESTAMP NOT NULL,
  way_id          INTEGER
    CONSTRAINT fk_rails_c6fc2445d8
    REFERENCES ways
  wagon_id        INTEGER
    CONSTRAINT fk_rails_c6fc2445d8
    REFERENCES wagons
);

CREATE TABLE locomotives
(
  id              BIGSERIAL NOT NULL
    CONSTRAINT locomotives_pkey
    PRIMARY KEY,
  manufacturer    VARCHAR,
  locomotive_type VARCHAR,
  production_year INTEGER,
  created_at      TIMESTAMP NOT NULL,
  updated_at      TIMESTAMP NOT NULL,
);

CREATE TABLE crews
(
  id         BIGSERIAL NOT NULL
    CONSTRAINT crews_pkey
    PRIMARY KEY,
  role       VARCHAR,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL,
);

CREATE TABLE employees
(
  id         BIGSERIAL NOT NULL
    CONSTRAINT employees_pkey
    PRIMARY KEY,
  first_name VARCHAR,
  last_name  VARCHAR,
  birth_date DATE,
  start_date DATE,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL,
);

CREATE TABLE employee_crews
(
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL,
  crew_id    INTEGER
    CONSTRAINT fk_rails_988e1936dc
    REFERENCES crews
  employee_id    INTEGER
    CONSTRAINT fk_rails_988e1937dc
    REFERENCES employees
);

CREATE TABLE feedbacks
(
  satisfaction INTEGER,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL,
  passenger_id    INTEGER
    CONSTRAINT fk_rails_988e1936dc
    REFERENCES passengers
  way_id    INTEGER
    CONSTRAINT fk_rails_988e1937dc
    REFERENCES ways
);

CREATE TABLE tracks
(
  id         BIGSERIAL NOT NULL
    CONSTRAINT tracks_pkey
    PRIMARY KEY,
  duration   INTEGER,
  track_type VARCHAR,
  number     INTEGER,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL
);
CREATE TABLE work_histories
(
  from DATETIME,
  to DATETIME,
  created_at TIMESTAMP NOT NULL,
  updated_at TIMESTAMP NOT NULL,
  way_id    INTEGER
    CONSTRAINT fk_rails_988e1937dc
    REFERENCES ways
  employee_id    INTEGER
    CONSTRAINT fk_rails_988e1936dc
    REFERENCES employees
);
