# dbs2018-project-szabo_kocan
Téma projektu: Železničná stanica
Autori: Richard Szabó, Kristián Kocan

**1. Zadanie**

Vo vami zvolenom prostredí vytvorte databázovú aplikáciu, ktorá komplexne rieši nižšie definované scenáre vo vami zvolenej doméne. Presný rozsah a konkretizáciu scenárov si dohodnete s Vašim cvičiacim na cvičení. Projekt sa rieši vo dvojiciach, pričom sa očakáva, že na synchronizáciu práce so spolužiakom / spolužiačkou použijete git.

*Zvolená doména: Železničná stanica*

Našou doménou bude databázový systém Železničných staníc v ktorom evidujeme zamestnancov pre jednotlivé posádky, vozne, rušne a pasažierov pre jednotlivé vlakové cesty. Tie sú modelované pre linky, ktoré sa skladajú z konkrétnych zastávok, ktoré tvoria stanice. 

* Employee/Zamestnanec - Zamestnanec, a jeho osobné údaje, dátum, kedy nastúpil do zamestnania, a id posadky(crew), ku ktorej patri
* Crew/Posadka - Posadka, obsahuje nazov roly a id cesty ku ktorej prislucha
* Wagon/Vozen - Vozen, obsahuje specifikaciu jednotlivych voznov a id cesty ku ktorej prislucha
* Locomotive/Rusen - Rusen, obsahuje specifikaciu jednotlivych rusnov a id cesty ku ktorej prislucha
* Passenger/Pasazier - Pasazier, obsahuje osobne udaje a id cesty ku ktorej prislucha
* Way/Cesta - Cesta, obsahuje meskanie jednotlivych ciest a id linky ku ktorej prislucha
* Track/Linka - Linka, obsahuje trvanie jednotlivych liniek, cislo linky a typ linky
* Stop/Zastavka - Zastavka, obsahuje poradie zastavky pre jednotlivu linku a id prisluchajucej stanice
* Station/Stanica - Stanica, obsahuje nazov, a informaciu ci sa v stanici daju zakupit listky

**2. Diagram fyzického dátového modelu**

![model](/doc/DatovyModel.png)

Zmeny oproti modelu z 2.odovzdania: 

* Domodelovaná časová následnosť
* Pridane prepojenie medzi Wagons a Ways ako aj Passengers a Ways
* Opravené kardinality napr. medzi Employees a Crews
* Pridané väzobné tabuľky wagon_usages, feedbacks, work_histories, employee_crews

**3. Špecifikácia scenárov**

Všeobecné scenáre:

1. Zobrazenie prehľadu všetkých Liniek s informáciami o trvaní cesty, roku výroby lokomotívy, spokojnosti cestujúcich, type linky ...
Rozširujúci podscenár: Filtrovanie záznamov spĺňajúcich ľubovoľný počet kritérií zadaných používateľom - výrobca lokomotívy ,... .
Používateľ má možnosť filtrovať záznamy.
![model](/doc/DetailTracks.png)

SELECT l.manufacturer as lman,l.production_year as lprod,t.duration as tdur,f.satisfaction as fas, t.track_type as trck_type, t.created_at as date FROM ways
JOIN locomotives l ON ways.locomotive_id = l.id
JOIN tracks t ON ways.track_id = t.id
JOIN feedbacks f ON ways.id = f.way_id
WHERE (l.manufacturer LIKE '" + @search + "');

2. Zobrazenie prehľadu všetkých Ciest s informáciami o Type cesty, čísle vlaku, trvaní cesty, počte zastávok, počte vagónov - na výpočet sa využíva GROUP BY. Používateľ má možnosť iba prehliadať záznamy.
![model](/doc/ReportCesta.png)

SELECT ways.id as wid,tracks.track_type as ttt,tracks.number as trn,tracks.duration as trd,ways.delay as wde, locomotives.locomotive_type as lolt,locomotives.manufacturer as loma, COUNT(wagon_usages.id) as ccc
FROM ways
  JOIN tracks ON ways.track_id = tracks.id
  JOIN locomotives ON ways.locomotive_id = locomotives.id
  JOIN wagon_usages ON wagon_usages.way_id = ways.id
  JOIN wagons ON wagon_usages.wagon_id = wagons.id
GROUP BY ways.id,tracks.track_type,tracks.number,tracks.duration,ways.delay, locomotives.locomotive_type,locomotives.manufacturer;

3. Zobrazenie konkrétneho záznamu - detail na zamestnanca. Pri zamestnancovi dopočítaná informácia o počte odpracovaných rokov.
Používateľ má možnosť upravovať údaje o zamestnancovi po kliknutí na Edit.
![model](/doc/DetailNaZamestnanca.png)

4. Vytvorenie nového záznamu - Pridanie nového vagóna do tabuľky vagónov.
Používateľ má možnosť pridať nový záznam - nový vagón do tabuľky wagons po kliknutí na New wagon.
![model](/doc/AktualizaciaVymazanie.png)

5. Aktualizácia existujúceho záznamu - Možnosť Edit pri záznamoch o Vagónoch alebo pri Zamestnancoch. 
6. Vymazanie záznamu - Možnosť Destroy pri záznamoch napr. v tabuľke vagónov.
Používateľ má možnosť vymazať záznam po kliknutí na Destroy alebo upraviť záznam po kliknutí na Edit.
![model](/doc/AktualizaciaVymazanie.png)

**4. Optimalizácia**

Databáza bola optimalizovaná prostredníctvom umiestnenia Indexov ako aj prostredníctvom vhodnej implementácie SQL Selectov, t.j. boli využité JOIN, GROUP BY a pod. na počítanie niektorých hodnôt. 

**5. Technická dokumentácia**

Na implementáciu sme použili skriptovací objektovo orientovaný programovací jazyk Ruby a jeho MVC framework Ruby on Rails. Ako prostredie pre implementáciu sme zvolili IDE RubyMine by JetBrains. Nakoľko bol použitý MVC framework Ruby on Rails, kód je členený  na triedy podľa MVC na Models, Views a Controllers.






