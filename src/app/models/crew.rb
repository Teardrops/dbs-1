class Crew < ApplicationRecord
  has_many :employee_crews, dependent: :destroy
end
