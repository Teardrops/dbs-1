class Track < ApplicationRecord
  has_many :ways, dependent: :destroy
  has_many :stops, dependent: :destroy
end
