class Employee < ApplicationRecord
  has_many :work_histories, dependent: :destroy
  has_many :employee_crews, dependent: :destroy
end
