class Way < ApplicationRecord
  belongs_to :track
  belongs_to :locomotive
  has_many :feedbacks, dependent: :destroy
  has_many :work_histories, dependent: :destroy
  has_many :wagon_usage, dependent: :destroy
end
