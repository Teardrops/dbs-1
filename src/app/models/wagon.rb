class Wagon < ApplicationRecord
  has_many :wagon_usage, dependent: :destroy
end
