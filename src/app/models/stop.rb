class Stop < ApplicationRecord
  belongs_to :track
  belongs_to :station
end
