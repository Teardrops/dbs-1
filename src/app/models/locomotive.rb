class Locomotive < ApplicationRecord
  has_many :ways, dependent: :destroy
end
