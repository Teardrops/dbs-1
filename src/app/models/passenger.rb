class Passenger < ApplicationRecord

  has_many :feedbacks, dependent: :destroy
end
