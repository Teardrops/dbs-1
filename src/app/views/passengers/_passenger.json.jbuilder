json.extract! passenger, :id, :first_name, :last_name, :birth_date, :entry_station, :exit_station, :created_at, :updated_at
json.url passenger_url(passenger, format: :json)
