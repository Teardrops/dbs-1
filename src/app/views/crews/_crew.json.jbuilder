json.extract! crew, :id, :role, :created_at, :updated_at
json.url crew_url(crew, format: :json)
