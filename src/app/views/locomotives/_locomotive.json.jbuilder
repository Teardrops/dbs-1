json.extract! locomotive, :id, :manufacturer, :locomotive_type, :production_year, :created_at, :updated_at
json.url locomotive_url(locomotive, format: :json)
