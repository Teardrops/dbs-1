json.extract! track, :id, :duration, :track_type, :number, :created_at, :updated_at
json.url track_url(track, format: :json)
