json.extract! wagon, :id, :production_year, :wagon_type, :created_at, :updated_at
json.url wagon_url(wagon, format: :json)
