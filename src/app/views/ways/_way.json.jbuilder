json.extract! way, :id, :delay, :created_at, :updated_at, :track_id,
              :locomotive_id
json.url way_url(way, format: :json)
