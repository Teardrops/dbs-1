json.extract! station, :id, :label, :ticket_sale, :created_at, :updated_at
json.url station_url(station, format: :json)
