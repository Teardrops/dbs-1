class LocomotivesController < ApplicationController
  before_action :set_locomotive, only: [:show, :edit, :update, :destroy]

  # GET /locomotives
  # GET /locomotives.json
  def index
    @locomotives = Locomotive.all.paginate(:page => params[:page], :per_page => 50)
  end

  # GET /locomotives/1
  # GET /locomotives/1.json
  def show
  end

  # GET /locomotives/new
  def new
    @locomotive = Locomotive.new
  end

  # GET /locomotives/1/edit
  def edit
  end

  # POST /locomotives
  # POST /locomotives.json
  def create
    Locomotive.transaction do
    @locomotive = Locomotive.new(locomotive_params)

    respond_to do |format|
      if @locomotive.save
        format.html { redirect_to @locomotive, notice: 'Locomotive was successfully created.' }
        format.json { render :show, status: :created, location: @locomotive }
      else
        format.html { render :new }
        format.json { render json: @locomotive.errors, status: :unprocessable_entity }
      end
    end
    end
    end

  # PATCH/PUT /locomotives/1
  # PATCH/PUT /locomotives/1.json
  def update
    Locomotive.transaction do
    respond_to do |format|
      if @locomotive.update(locomotive_params)
        format.html { redirect_to @locomotive, notice: 'Locomotive was successfully updated.' }
        format.json { render :show, status: :ok, location: @locomotive }
      else
        format.html { render :edit }
        format.json { render json: @locomotive.errors, status: :unprocessable_entity }
      end
    end
      end
  end

  # DELETE /locomotives/1
  # DELETE /locomotives/1.json
  def destroy
    Locomotive.transaction do
    @locomotive.destroy
    respond_to do |format|
      format.html { redirect_to locomotives_url, notice: 'Locomotive was successfully destroyed.' }
      format.json { head :no_content }
    end
      end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_locomotive
      @locomotive = Locomotive.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def locomotive_params
      params.require(:locomotive).permit(:manufacturer, :locomotive_type, :production_year)
    end
end
