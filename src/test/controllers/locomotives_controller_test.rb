require 'test_helper'

class LocomotivesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @locomotive = locomotives(:one)
  end

  test "should get index" do
    get locomotives_url
    assert_response :success
  end

  test "should get new" do
    get new_locomotive_url
    assert_response :success
  end

  test "should create locomotive" do
    assert_difference('Locomotive.count') do
      post locomotives_url, params: { locomotive: { locomotive_type: @locomotive.locomotive_type, manufacturer: @locomotive.manufacturer, production_year: @locomotive.production_year } }
    end

    assert_redirected_to locomotive_url(Locomotive.last)
  end

  test "should show locomotive" do
    get locomotive_url(@locomotive)
    assert_response :success
  end

  test "should get edit" do
    get edit_locomotive_url(@locomotive)
    assert_response :success
  end

  test "should update locomotive" do
    patch locomotive_url(@locomotive), params: { locomotive: { locomotive_type: @locomotive.locomotive_type, manufacturer: @locomotive.manufacturer, production_year: @locomotive.production_year } }
    assert_redirected_to locomotive_url(@locomotive)
  end

  test "should destroy locomotive" do
    assert_difference('Locomotive.count', -1) do
      delete locomotive_url(@locomotive)
    end

    assert_redirected_to locomotives_url
  end
end
