Rails.application.routes.draw do
  resources :homes
  root 'homes#index'
  resources :tracks
  resources :employees
  resources :crews
  resources :locomotives
  resources :wagons
  resources :ways
  resources :passengers
  resources :stops
  resources :stations
  get '/report_wagon' , to: 'wagons#report'
  get '/report_way' , to: 'ways#report'
  get '/report_1' , to: 'homes#track_delay'
  get '/report_2' , to: 'homes#report2'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
