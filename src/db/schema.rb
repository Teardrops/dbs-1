# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180429143225) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "crews", force: :cascade do |t|
    t.string "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "employee_crews", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "employee_id"
    t.integer "crew_id"
  end

  create_table "employees", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.date "birth_date"
    t.date "start_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "feedbacks", force: :cascade do |t|
    t.integer "satisfaction"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "passenger_id"
    t.integer "way_id"
  end

  create_table "homes", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "locomotives", force: :cascade do |t|
    t.string "manufacturer"
    t.string "locomotive_type"
    t.integer "production_year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "passengers", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.date "birth_date"
    t.integer "entry_station"
    t.integer "exit_station"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stations", force: :cascade do |t|
    t.string "label"
    t.boolean "ticket_sale"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "stops", force: :cascade do |t|
    t.integer "order"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "station_id"
    t.integer "track_id"
  end

  create_table "tracks", force: :cascade do |t|
    t.integer "duration"
    t.string "track_type"
    t.integer "number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "wagon_usages", force: :cascade do |t|
    t.datetime "from"
    t.datetime "to"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "way_id"
    t.integer "wagon_id"
  end

  create_table "wagons", force: :cascade do |t|
    t.string "wagon_type"
    t.integer "production_year"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ways", force: :cascade do |t|
    t.integer "delay"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "track_id"
    t.integer "locomotive_id"
  end

  create_table "work_histories", force: :cascade do |t|
    t.datetime "from"
    t.datetime "to"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "way_id"
    t.integer "employee_id"
  end

  add_foreign_key "employee_crews", "crews"
  add_foreign_key "employee_crews", "employees"
  add_foreign_key "feedbacks", "passengers"
  add_foreign_key "feedbacks", "ways"
  add_foreign_key "stops", "stations"
  add_foreign_key "stops", "tracks"
  add_foreign_key "wagon_usages", "wagons"
  add_foreign_key "wagon_usages", "ways"
  add_foreign_key "ways", "locomotives"
  add_foreign_key "ways", "tracks"
  add_foreign_key "work_histories", "employees"
  add_foreign_key "work_histories", "ways"
end
