class CreateWays < ActiveRecord::Migration[5.1]
  def change
    create_table :ways do |t|
      t.integer :delay

      t.timestamps
    end
  end
end
