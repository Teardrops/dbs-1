class AddForeignKeys2 < ActiveRecord::Migration[5.1]
  def change

    # FK v work_histories
    add_column :work_histories, :way_id, :integer
    add_foreign_key :work_histories, :ways
    add_column :work_histories, :employee_id, :integer
    add_foreign_key :work_histories, :employees

    # FK v employee_crews
    add_column :employee_crews, :employee_id, :integer
    add_foreign_key :employee_crews, :employees
    add_column :employee_crews, :crew_id, :integer
    add_foreign_key :employee_crews, :crews

    # FK v wagon_usage
    add_column :wagon_usages, :way_id, :integer
    add_foreign_key :wagon_usages, :ways
    add_column :wagon_usages, :wagon_id, :integer
    add_foreign_key :wagon_usages, :wagons

    # FK v feedbacks
    add_column :feedbacks, :passenger_id, :integer
    add_foreign_key :feedbacks, :passengers
    add_column :feedbacks, :way_id, :integer
    add_foreign_key :feedbacks, :ways

  end
end
