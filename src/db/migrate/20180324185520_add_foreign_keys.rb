class AddForeignKeys < ActiveRecord::Migration[5.1]
  def change

    # FK v stops
    add_column :stops, :station_id, :integer
    add_column :stops, :track_id, :integer
    add_foreign_key :stops, :stations
    add_foreign_key :stops, :tracks

# FK v ways
    add_column :ways, :track_id, :integer
    add_foreign_key :ways, :tracks
    add_column :ways, :locomotive_id, :integer
    add_foreign_key :ways, :locomotives





  end
end
