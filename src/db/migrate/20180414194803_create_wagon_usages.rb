class CreateWagonUsages < ActiveRecord::Migration[5.1]
  def change
    create_table :wagon_usages do |t|
      t.datetime :from
      t.datetime :to

      t.timestamps
    end
  end
end
