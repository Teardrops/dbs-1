class CreateStations < ActiveRecord::Migration[5.1]
  def change
    create_table :stations do |t|
      t.string :label
      t.boolean :ticket_sale

      t.timestamps
    end
  end
end
