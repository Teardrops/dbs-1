class CreateWorkHistories < ActiveRecord::Migration[5.1]
  def change
    create_table :work_histories do |t|
      t.datetime :from
      t.datetime :to

      t.timestamps
    end
  end
end
