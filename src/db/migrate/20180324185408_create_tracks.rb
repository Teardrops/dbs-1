class CreateTracks < ActiveRecord::Migration[5.1]
  def change
    create_table :tracks do |t|
      t.integer :duration
      t.string :track_type
      t.integer :number

      t.timestamps
    end
  end
end
