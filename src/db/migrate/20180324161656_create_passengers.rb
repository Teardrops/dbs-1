class CreatePassengers < ActiveRecord::Migration[5.1]
  def change
    create_table :passengers do |t|
      t.string :first_name
      t.string :last_name
      t.date :birth_date
      t.integer :entry_station
      t.integer :exit_station

      t.timestamps
    end
  end
end
