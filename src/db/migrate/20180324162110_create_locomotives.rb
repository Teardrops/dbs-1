class CreateLocomotives < ActiveRecord::Migration[5.1]
  def change
    create_table :locomotives do |t|
      t.string :manufacturer
      t.string :locomotive_type
      t.integer :production_year

      t.timestamps
    end
  end
end
