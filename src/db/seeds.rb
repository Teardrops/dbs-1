# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require 'faker'

1000000.times do
  Wagon.create(
      wagon_type: Faker::WorldOfWarcraft.hero,
      production_year: Faker::Number.between(1900,2018),
      )
end

1000000.times do
  Passenger.create(
      first_name: Faker::Name.first_name,
      last_name: Faker::Name.last_name,
      birth_date: Faker::Date.birthday,
      entry_station: Faker::Number.between(1,25),
      exit_station: Faker::Number.between(1,25),
  )
end
1000000.times do
  Locomotive.create(
      manufacturer: Faker::Vehicle.manufacture,
      locomotive_type: Faker::GameOfThrones.dragon,
      production_year: Faker::Number.between(1980,2018),
  )
end
1000000.times do
  Station.create(
    label: Faker::Address.city,
    ticket_sale: Faker::Boolean.boolean
  )
end
1000000.times do
  Crew.create(
      role: Faker::Job.field,
  )
end
2000000.times do
  Employee.create(
      first_name: Faker::Name.first_name,
      last_name: Faker::Name.last_name,
      birth_date: Faker::Date.birthday,
      start_date: Faker::Date.backward(730),
      )
end
1000000.times do
  Track.create(
    duration: Faker::Number.between(20,600),
    track_type: Faker::Space.galaxy,
    number: Faker::Number.number(3)
  )
end
1000000.times do
  Stop.create(
      order: Faker::Number.between(1,25),
      station_id: Faker::Number.between(1,1000000),
      track_id: Faker::Number.between(1,1000000)
  )
end
1000000.times do
  Way.create(
    delay: Faker::Number.between(1, 120),
    track_id: Faker::Number.between(1,1000000),
    locomotive_id: Faker::Number.between(1,1000000),
      )
end
1000000.times do
  WagonUsage.create(
      from:Faker::Date.backward(730),
      to:Faker::Date.backward(730),
      way_id: Faker::Number.between(1,1000000),
      wagon_id: Faker::Number.between(1,1000000),
      )
end
1000000.times do
  EmployeeCrew.create(
      employee_id: Faker::Number.between(1,1000000),
      crew_id: Faker::Number.between(1,1000000),
      )
end
1000000.times do
  Feedback.create(
      satisfaction: Faker::Number.between(1, 100),
      way_id: Faker::Number.between(1,1000000),
      passenger_id: Faker::Number.between(1,1000000),
      )
end
1000000.times do
  WorkHistory.create(
      from:Faker::Date.backward(730),
      to:Faker::Date.backward(730),
      way_id: Faker::Number.between(1,1000000),
      employee_id: Faker::Number.between(1,1000000),
      )
end


